core = 7.x
api = 2

libraries[spin][download][type] = "git"
libraries[spin][download][url] = "https://github.com/fgnass/spin.js.git"
libraries[spin][download][tag] = v2.0.2
libraries[spin][destination] = "libraries"
libraries[spin][directory_name] = "spin"
libraries[spin][type] = "library"

