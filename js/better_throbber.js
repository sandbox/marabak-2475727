(function ($) {
  if(typeof Drupal.ajax != 'undefined' && typeof Drupal.ajax.prototype != 'undefined'){
    Drupal.ajax.prototype.original_beforeSend = Drupal.ajax.prototype.beforeSend;

    Drupal.ajax.prototype.beforeSend = function (xmlhttprequest, options) {
      this.original_beforeSend(xmlhttprequest, options);

      if (this.progress.type == 'better_throbber') {
        this.progress.element = $('<div class="ajax-progress ajax-progress-throbber"><div class="better-throbber">&nbsp;</div></div>');
        if (this.progress.message) {
          $('.better-throbber', this.progress.element).after('<div class="message">' + this.progress.message + '</div>');
        }
        $(this.element).after(this.progress.element);


        var opts = {
          lines: 12, // The number of lines to draw
          length: 5, // The length of each line
          width: 2, // The line thickness
          radius: 3, // The radius of the inner circle
          corners: 1, // Corner roundness (0..1)
          rotate: 0, // The rotation offset
          direction: 1, // 1: clockwise, -1: counterclockwise
          color: '#fff', // #rgb or #rrggbb or array of colors
          speed: 1, // Rounds per second
          trail: 44, // Afterglow percentage
          shadow: false, // Whether to render a shadow
          hwaccel: false, // Whether to use hardware acceleration
          className: 'spinner', // The CSS class to assign to the spinner
          zIndex: 2e9, // The z-index (defaults to 2000000000)
          top: '0%', // Top position relative to parent
          left: '0%' // Left position relative to parent
        };
        var spinner = new Spinner(opts).spin();
        this.progress.element.append(spinner.el);
      }
    };
  }
})(jQuery);
